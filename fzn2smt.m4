  ###############
  # download fzn2smt
  ###############

COPY ./docker_scripts/antlr-runtime-3.2.jar /antlr-runtime-3.2.jar

RUN ( [ -d /solvers_exec ] || mkdir /solvers_exec ) && \
	###############
	# install needed packages
	###############  
	apt-get update && \
	# openjdk and unzip for the fzn2smt	
	apt-get install -y \
		unzip \
		openjdk-8-jdk && \
	rm -rf /var/lib/apt/lists/* && \
  cd /solvers_exec && \
  wget http://ima.udg.edu/recerca/lap/fzn2smt/version/fzn2smt-2-0-02.zip && \
  unzip fzn2smt-2-0-02.zip && \
  rm -rf fzn2smt-2-0-02.zip

ENV CLASSPATH=/solvers_exec/fzn2smt-2-0-02:/antlr-runtime-3.2.jar:$CLASSPATH

