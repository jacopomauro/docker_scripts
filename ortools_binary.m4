  ###############
  # download or-tools and handle global def
  ###############
RUN ( [ -d /solvers_exec ] || mkdir /solvers_exec ) && \
	cd /solvers_exec && \
  wget https://github.com/google/or-tools/releases/download/v2015-12/Google.OrTools.flatzinc.Linux64.3393.tar.gz && \
  tar -zxvf Google.OrTools.flatzinc.Linux64.3393.tar.gz && \
  rm -rf Google.OrTools.flatzinc.Linux64.3393.tar.gz && \
  ln -s /solvers_exec/or-tools.Linux64/bin/fzn-or-tools /bin/fzn-ortools && \
  ( [ -d /solvers_exec/MiniZincIDE ] && \
	  ln -s /solvers_exec/or-tools.Linux64/share/minizinc /solvers_exec/MiniZincIDE/share/minizinc/ortools || \
		echo MiniZincIde not installed ) && \
  ( [ -d /solvers_exec/minisearch ] && \
	ln -s /solvers_exec/or-tools.Linux64/share/minizinc /solvers_exec/minisearch/share/minizinc/ortools || \
		echo MiniSearch not installed )



  ###############
  # compile ortools (right now not done, takes too much time)
  ###############
  # alias svn="svn --non-interactive --trust-server-cert"

